﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Alturos.Yolo;
using Alturos.Yolo.Model;
using Emgu.CV;

namespace YoloWebcam
{
    public partial class Form1 : Form
    {
        VideoCapture capture;
        YoloWrapper yolo;
        List<string> types;

        public Form1()
        {
            InitializeComponent();
            Run();
        }

        private void Run()
        {
            try
            {
                capture = new VideoCapture();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
                return;
            }

            yolo = new YoloWrapper("yolov2-tiny.cfg", "yolov2-tiny.weights", "coco.names");
            //yolo = new YoloWrapper("yolov3-tiny.cfg", "yolov3-tiny.weights", "coco.names");

            var namesFile = File.ReadAllLines("coco.names");
            types = new List<string>(namesFile);

            Application.Idle += ProcessFrame;
        }

        private void ProcessFrame(object sender, EventArgs e)
        {
            //var img = capture.QuerySmallFrame(); //replace with unity color camera
            var img = capture.QueryFrame(); //replace with unity color camera
            pictureBox1.Image = img.Bitmap;
            Detect();
        }

        private void Detect()
        {
            MemoryStream memoryStream = new MemoryStream();
            pictureBox1.Image.Save(memoryStream, ImageFormat.Png);
            var items = yolo.Detect(memoryStream.ToArray()).ToList();
            AddDetailsToPictureBox(pictureBox1, items);
        }

        private void AddDetailsToPictureBox(PictureBox pictureBox, List<YoloItem> items)
        {
            var img = pictureBox.Image;
            var font = new Font("Arial", 18, FontStyle.Regular);
            var brush = new SolidBrush(Color.Red);
            var graphics = Graphics.FromImage(img);

            foreach (var item in items)
            {
                var x = item.X;
                var y = item.Y;
                var width = item.Width;
                var hight = item.Height;
                var type = item.Type;
                var rect = new Rectangle(x, y, width, hight);
                var pen = new Pen(Color.FromArgb(unchecked(255 * 255 * 255 * 255 / (types.IndexOf(type) + 1))), 2);
                var point = new Point(x, y);

                graphics.DrawRectangle(pen, rect);
                graphics.DrawString(type, font, brush, point);
            }
            pictureBox.Image = img;
        }

    }
}
