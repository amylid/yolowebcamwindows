This is the prototype for the implementation of Yolo in C#.
This version uses the computer webcam for the object detection.

Include the following NuGet packages to compile:
    Alturos.Yolo v2.6.4 (https://github.com/AlturosDestinations/Alturos.Yolo)
    Emgu.CV v3.4.3 (http://www.emgu.com/wiki/index.php/Main_Page)